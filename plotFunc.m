function plotFunc(m, p, freqvector, s, f, t, pdfName)
% Plot DFT
    m_plot = m(1:length(m) / 2);    % Make sure the length of magnitude is the same as freqvector
    p_plot = p(1:length(p) / 2);    % Make sure the length of phase is the same as freqvector

    figure;
    %Subplot for DFT magnitude and phase, and STFT (spectogram)
    subplot(3,1,1)  
    plot(freqvector, m_plot)    % Plot magnitude
    title('Magnitude')
    xlabel('Frequency')
    ylabel('dB')
    xlim([0 1000])
    ylim([0 100])
    
    subplot(3,1,2)
    plot(freqvector, p_plot*180/pi) % Plot phase
    title('Phase')
    xlabel('Frequency')
    ylabel('angle in degrees')
    xlim([0 1000])
    
    subplot(3,1,3)
    imagesc(t, f, mag2db(abs(s))); % Convert magnitude to dB for better visualization
    axis xy; % To have the low frequencies at the bottom
    title('STFT');
    xlabel('Time (s)');
    ylabel('Frequency (Hz)');
    colorbar;
    ylim([0, 1000]);
    print(pdfName, '-dpdf')
end