from PyPDF2 import PdfMerger

pdf_all = ['testPlotfunc.pdf', 'poleZeroplot.pdf','frequencyResponse.pdf']
merger = PdfMerger()

for pdf in pdf_all:
    merger.append(pdf)

merger.write("result.pdf")
merger.close()