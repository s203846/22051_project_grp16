% fs = sampling frequency
% duration = duration of the signal
% windowtype = The type of window which needs to be applied to the signal
% signal = input signal
function windowedSignal = windowing(fs, duration, windowType, signal)

% Switch case to deterime the correct type of window from the global_specs
% file
% The window is being applied to the signal by convolution between the
% signal and the window
switch windowType
    case 'hann'
        window = hann(fs*duration);
        windowedSignal = conv(window,signal);
    case 'hamming'
        window = hamming(fs*duration);
        windowedSignal = conv(window,signal);

    case 'rect'
        window = rectwin(fs*duration);
        windowedSignal = conv(window,signal);
    otherwise
        warning('Unexpected windowtype. No window created')
end
end