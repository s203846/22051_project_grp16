function signal_with_overlap = applyOverlap(signal, window, overlap)
    buffer_size = length(window);
    hop_size = buffer_size - overlap;
    num_buffers = floor((length(signal) - overlap) / hop_size);
    
    signal_with_overlap = zeros(1, length(signal)); %Zero padding signal
    
    %For loop for crunching/Truncating signal
    for i = 1:num_buffers
        start_idx = (i - 1) * hop_size + 1;
        end_idx = start_idx + buffer_size - 1;
        signal_with_overlap(start_idx:end_idx) = signal_with_overlap(start_idx:end_idx) + signal(start_idx:end_idx) .* window; %Truncating signal with window function
    end
end