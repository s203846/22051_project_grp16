function window = getWindowFunction(windowType, window_length)
    switch lower(windowType)
        case 'rect'
            window = rectwin(window_length)';
        case 'hann'
            window = hann(window_length, 'periodic')';
        case 'hamming'
            window = hamming(window_length, 'periodic')';
        otherwise
            error('Unsupported window type');
    end
end