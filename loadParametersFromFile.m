function params = loadParametersFromFile(filename)
% Define a struct with fields for all expected parameters
params = struct('inputType', [], 'wavFileName', [], 'targetSamplingFreq', [], ...
    'signalType', [], 'samplingFreq', [], 'signalDuration', [], ...
    'periodicity', [], 'toneFrequencies', [], 'spectralResolution', [], ...
    'windowType', [], 'overlapPercent', [], 'filterType', [], ...
    'filterOrder', [], 'approxMethod', [], 'filterDesign', [], ...
    'cutoffFrequencies', [], 'timeAxisLimit', [], 'amplitudeAxisLimit', [], ...
    'freqAxisScale', [], 'amplitudeScale', [], 'freqAxisLimit', []);

% Open the specification file
fid = fopen(filename, 'r');

%If file cannot be found, print error
if fid == -1
error('Cannot open specification file.');
end
%Define lines that contains value with given names
lineNumbers = struct('inputType', 3, 'wavFileName', 5, 'targetSamplingFreq', 7, ...
         'signalType', 10, 'samplingFreq', 12, 'signalDuration', 14, ...
         'periodicity', 16, 'toneFrequencies', 18, 'spectralResolution', 21, ...
         'windowType', 23, 'overlapPercent', 25, 'filterType', 28, ...
         'filterOrder', 30, 'approxMethod', 32, 'filterDesign', 34, ...
         'cutoffFrequencies', 36, 'timeAxisLimit', 40, 'amplitudeAxisLimit', 42, ...
         'freqAxisScale', 45, 'amplitudeScale', 47, 'freqAxisLimit', 49, 'ylim', 51);
%Set line at top
currentLine = 0;

while ~feof(fid)
currentLine = currentLine + 1;
line = fgetl(fid); % Read one line
if any(structfun(@(x) x == currentLine, lineNumbers))

fieldName = fieldnames(lineNumbers);
fieldName = fieldName{structfun(@(x) x == currentLine, lineNumbers)};
value = strtrim(line);
%Converts string to double
numValue = str2double(value);
if ~isnan(numValue)
value = numValue;
elseif contains(value, ',')
value = str2num(value);
end

params.(fieldName) = value;
end
end

% Close the file
fclose(fid);
end