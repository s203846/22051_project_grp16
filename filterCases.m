
% The function takes 4 parameters: 
% order = filter order,
% appMeth = Approximation method. 
% Possible: butterworth, chebychevI, chebychevII, cauer
% fType = filter type ('low' = lowpass, 'bandpass' = bandpass, 
% 'high' = highpass, 'stop' = bandstop filter
% cutOffFreq = Cut off frequency, can be a scalar for LP, HP and a
% 2D-vector for BP, BS
% sampFreq = sampling frequency (Hz)
function [z,p,k] = filterCases(order, appMeth, fType, cutOffFreq, sampFreq, filterType)

% The ripples in the cheby- and cauer filters. The ripples in the stopband
% for the cauer filter has to be greater than in the passband
ripCheb1 = 2;
ripCheb2 = 2;
ripPassCauer = 2;
ripStopCauer = 4;

%The filters need a normalized cut off frequency (has to be between 0-1)
normCutOff = cutOffFreq/(sampFreq/2);
% Converts the data from the global_specs file into the format needed for
% the filters
switch fType
    case 'lp'
        fType = 'low';
    case 'hp'
        fType = 'high';
    case 'bp'
        fType = 'bandpass';
    case 'bs'
        fType = 'stop';
end

% Finds the filter coefficients (zeros, poles and gain) in a switch case
% determined by the global_specs file
switch filterType
    case 'fir'
        z = fir1(order, normCutOff, fType, 'scale');
        p = ones(1, length(z));
        k = 1;
    case 'iir'
        switch appMeth
            case 'butterworth'
                [z, p, k] = butter(order, normCutOff, fType);
            case 'chebychevI'
                [z, p, k] = cheby1(order, ripCheb1, normCutOff, fType);
            case 'chebychevII'
                [z, p, k] = cheby2(order, ripCheb2, normCutOff, fType);
            case 'cauer'
                [z, p, k] = ellip(order, ripPassCauer, ripStopCauer, normCutOff, fType);
            otherwise
                warning('Unexpected filtertype. No filter created')
                z = null; p = null; k = null;
        end
end

% Plots the transfer function of the approximations and the pole/zeros of
% the filter
figure;
plotFilter(z,p,k,normCutOff,'frequencyResponse.pdf','poleZeroplot.pdf');
% Uses bilinear transform to do a analog-to-digital filter conversion
%[zd,pd,kd] = bilinear(z,p,k,sampFreq);
end





