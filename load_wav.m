function wave = load_wav(path2wav, fname)

[y, fs_y] = audioread([path2wav fname]);

wave = [y, fs_y];
end


