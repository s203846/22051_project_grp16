filename = 'global_specs.txt';
params = loadParametersFromFile(filename);

%Uncomment code to show parsed parameters from the global_specs file
%disp('Parsed parameters:');
%disp(params);

%Load signal, either generate or wav
if strcmp(params.inputType, 'wav')
    signal = process_wav(params.wavFileName, params.targetSamplingFreq);
elseif strcmp(params.inputType, 'generate')
    [signal, time_vector] = generate_signal(params.samplingFreq, params.toneFrequencies, params.signalDuration, params.periodicity, params.signalType);
end
% Frequency analysis and plot 
[freqanalysisresults, freqvector, s, f, t] = freqAnalysis(signal, params.samplingFreq, params.spectralResolution, params.overlapPercent, params.windowType);

% Description in the function
[z,p,k] = filterCases(params.filterOrder,params.approxMethod,params.filterDesign,params.cutoffFrequencies,params.samplingFreq, params.filterType);

figure;
impz(z, p)

if strcmp(params.filterType, 'fir')
    % For FIR filters, the filter coefficients are in 'z'.
    % Since FIR filters do not use the 'p' or 'k' values, we use 1 for the second argument of 'filter'.
    outputSignal = filter(z, 1, signal);

elseif strcmp(params.filterType, 'iir')
    % For IIR filters, we need to convert zeros, poles, and gain (z, p, k) to transfer function coefficients.
    % This is done using the zp2tf function.
    [b, a] = zp2tf(z, p, k);
    outputSignal = filter(b, a, signal);

end


% Plot the original and filtered signals
figure;
plot(time_vector, signal);
hold on
plot(time_vector, outputSignal);
legend("original signal", "filtered signal")
xlim([0 params.timeAxisLimit])
ylim([-params.amplitudeAxisLimit params.amplitudeAxisLimit])





