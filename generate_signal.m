function [signal, time_vector] = generate_signal(fs, fn, duration, periodicity, signalType)

    time_vector = 0:1/fs:duration-1/fs;
    switch signalType
        case 'rect'
            on = ones(1, 1/periodicity)*1/periodicity;  
            signal = [on zeros(1,fix((duration-periodicity)*fs))];
        case 'tone'
            signal = sin(2 * pi * fn(1) * time_vector);
        case 'tone-complex'
            signal = zeros(1,length(time_vector));
            for i = 1 :length(fn)
                signal = signal + sin(2*pi*fn(i)*time_vector);
            end    
        case 'noise'
            signal = randn(1, duration*fs);
    end
%Plotting signal in time domain
figure;
plot(time_vector, signal)
xlim([0 0.1])
ylim([-4 4])

end

