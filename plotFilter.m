
% The plotFilter function takes the z = zeros, p = poles, k = gain,
% cutOffFreq = cut off frequency as input
function plotFilter(z,p,k,cutOffFreq,pdfNameFreqz,pdfNameZplane)

% zp2sos converts the poles, zeros and gain to a 2d matrix
sos = zp2sos(z,p,k);

% freqz funtion is the frequency response of a digital filter
freqz(sos)

% Plots the cut off frequencies using a line
xline(cutOffFreq,Color=[0.8500 0.3250 0.0980])
print(pdfNameFreqz, '-dpdf')

% Limits the x-axis to normalized frequency and makes a grid in the plot
xlim([0 1])
grid
figure()

%Plots the zeros and the poles in the z-plane
zplane(z,p)
print(pdfNameZplane, '-dpdf')

end