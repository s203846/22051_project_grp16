function [freqanalysisresults, freqvector, s, f, t] = freqAnalysis(signal, samplingFreq, spectral_resolution, overlap_percentage, windowType)

    window_length = round(samplingFreq / spectral_resolution);  %calculate the length of the window signal
    overlap = round(overlap_percentage / 100 * window_length);  %Calculate the length of the overlap

    % Apply the selected window function with overlap
    window = getWindowFunction(windowType, window_length);  %Recall windowfunction, another script
    signal_with_overlap = applyOverlap(signal, window, overlap);    %Recall overlapfunction, another script

    freqvector = 0:samplingFreq/length(signal):samplingFreq/2 - 1 / samplingFreq; %frequency vector
    freqanalysisresults = fft(signal_with_overlap); %Do DFT analysis of signal with overlap from window function
    
    m = mag2db(abs(freqanalysisresults)); % magnitude in dB
    freqanalysisresults(m < 1e-6) = 0; % Signal is only available > 0
    p = unwrap(angle(freqanalysisresults)); % phase in radians

    [s, f, t] = spectrogram(signal_with_overlap, window_length, overlap, [], samplingFreq, 'yaxis');    %Do STFT of signal with overlap

    plotFunc(m, p, freqvector, s, f, t,'testPlotfunc.pdf');    %Recall plotfunction, subplot of DFT and STFT
end

